import 'package:flutter/material.dart';
import 'package:flutter_task/login.dart';
import 'package:flutter_task/third_page.dart';

class SecondPage extends StatefulWidget {
  String first_name = "";
  String last_name = "";
  String index = "";
  String url = "";

  SecondPage({this.first_name, this.last_name});

  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  @override
  Widget build(BuildContext context) {
    final _media = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
        height: double.infinity / 2,
        width: double.infinity / 2,
        decoration: BoxDecoration(
//          gradient: SIGNUP_BACKGROUND,
          color: Color(0xE9000000),
//          color: Color(0xE9FFFFFF),
        ),
    child: SingleChildScrollView(
    physics:  BouncingScrollPhysics(),
          child:Stack(
            children: <Widget>[
              Positioned(
                top: 10,
                right: 40,
                child: Image.asset(
                  './assets/images/dollar.png',
                  height: _media.height / 4,
                ),
              ),
              Stack(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 0, horizontal: _media.width / 20),
                    height: _media.height,
                    width: _media.width,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SizedBox(
                          height: _media.height / 50,
                        ),
                        IconButton(
                            icon: Icon(Icons.arrow_back),
                            color: Color(0xFFEBB395),
                            onPressed: () {
                              Navigator.of(context).pop();
                            }),

                        Container(
                          padding: EdgeInsets.all(0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Stack(
                                children: <Widget>[
                                  Text(
                                    'SELECT YOUR',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontFamily: "td-neumann",
                                      color: Colors.white,
                                      fontSize: 30,
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.fromLTRB(0,35,0,0),
                                    child: Text(
                                      'CERTIFICATE',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontFamily: "td-neumann",
                                        color: Color(0xFFEBB395),
                                        fontSize: 30,
                                      ),
                                    ),
                                  ),

                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(_media.width / 25, 0, 0,0),
                          child: SizedBox(
                            child: Image.asset("./assets/images/stick0.png",
                              width: 10,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(0, _media.height/4+10,0,0),
                    child:  Stack(
                      children: <Widget>[
                        Container(
//                    flex: 2, // 20%
                            height: _media.height/4,
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ThirdPage(
                                        first_name: widget.first_name,
                                        last_name: widget.last_name,
                                        index: "1",
                                        url: './assets/images/badge.png',
                                      )),
                                );
                              },
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                    vertical: 0, horizontal: _media.width / 20),
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment(0.8, 0.0),
                                    // 10% of the width, so there are ten blinds.
                                    colors: [
                                      const Color(0xFF372C29),
                                      const Color(0xFF1B1C20)
                                    ], // whitish to gray
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(0),
                                    bottomRight: Radius.circular(0),
                                  ),
                                  border: Border.all(
                                    width: 0.0,
                                  ),
                                ),

                                child:Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.fromLTRB(_media.width/25,0,0,0),
                                      child: Image.asset("./assets/images/stick1.png",
                                        width: 10.0,
                                        height: _media.height/4,
                                      ),
                                    ),
                                    Padding(
                                      padding:
                                      const EdgeInsets.symmetric(vertical: 0.0, horizontal: 10),
                                      child: (
                                          Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: <Widget>[
                                              Image.asset(
                                                './assets/images/badge.png',
                                                height: _media.height / 10,
                                                width: _media.width/5,
                                              ),
                                              SizedBox(
                                                height: _media.height / 50,
                                              ),
                                              Container(
                                                width: _media.width/5,
                                                padding: EdgeInsets.all(0),
                                                decoration: BoxDecoration(
                                                  gradient: LinearGradient(
                                                    begin: Alignment.topCenter,
                                                    end: Alignment(0, 0.8),
                                                    colors: [
                                                      const Color(0xFFEBB395),
                                                      const Color(0xFFBE744F)
                                                    ] , // whitish to gray
                                                  ),
                                                  borderRadius: BorderRadius.circular(5),
                                                ),
                                                child: Text(
                                                  'FREE',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    fontFamily: "td-neumann",
                                                    color: Colors.black,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )
                                      ),
                                    ),

                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
//                                                mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text(
                                          "CERTIFICATE",
                                          style: TextStyle(
                                            fontFamily: "td-neumann",
                                            color: Colors.white,
                                            fontSize: 20,
                                          ),
                                        ),
                                        SizedBox(
                                          height: _media.height/50,
                                        ),
                                        Container(
                                          width: _media.width*3/7,
                                          child: Text(
                                            "This is the one hell of a Didonesinspired - sturdy and \"mechanical\" Neumann.It all started with something good.",
                                            textAlign: TextAlign.justify,
                                            softWrap: true,
                                            maxLines: 10,
                                            style: TextStyle(
                                              color: Color(0xFF7B655A),
                                            ),
                                          ),
                                        ),


                                      ],
                                    ),

                                    Container(
                                      margin: EdgeInsets.fromLTRB(_media.width/25,0,0,0),
                                      padding: EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                        color: Color(0xFFBE744F),
                                        shape: BoxShape.circle,
                                        gradient: LinearGradient(
                                          begin: Alignment.topCenter,
                                          end: Alignment(0, 0.8),
                                          colors: [
                                            const Color(0xFFEBB395),
                                            const Color(0xFFBE744F)
                                          ] , // whitish to gray
                                        ),
                                      ),
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(vertical: 15,  horizontal: 0),
                                        child:  Text(
                                          '1',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            fontFamily: "td-neumann",
                                            color: Colors.black,
                                            fontSize: 25,
                                          ),
                                        ),
                                      ),

                                    ),
                                  ],

                                ),

                              ),
                            )
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, _media.height/4-10,0,0),
                          child: Stack(
                            children: <Widget>[
                              Container(
//                                height: _media.height/4,
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => ThirdPage(
                                              first_name: widget.first_name,
                                              last_name: widget.last_name,
                                              index: "2",
                                              url: './assets/images/Group.png',
                                            )),
                                      );
                                    },
                                    child: Container(
                                      height: _media.height / 4,
                                      margin: EdgeInsets.symmetric(
                                          vertical: 0, horizontal: _media.width / 20),
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          begin: Alignment.bottomRight,
                                          end: Alignment(0, 0),
                                          // 10% of the width, so there are ten blinds.
                                          colors: [
                                            const Color(0x99BE744F),
                                            const Color(0xFF372C29),
                                            /*  const Color(0xFF1B1C20),
                                          const Color(0xFFEEC20)*/
                                          ], // whitish to gray
                                        ),
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10),
                                          bottomLeft: Radius.circular(0),
                                          bottomRight: Radius.circular(0),
                                        ),
                                        border: Border.all(
                                          width: 0.0,
                                        ),
                                      ),
                                      child: Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.fromLTRB(_media.width/25,0,0,0),
                                            child: Image.asset("./assets/images/stick1.png",
                                              width: 10.0,
                                              height: _media.height/4,
                                            ),
                                          ),
                                          Padding(
                                            padding:
                                            const EdgeInsets.symmetric(vertical: 0.0, horizontal: 10),
                                            child: (
                                                Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Image.asset(
                                                      './assets/images/Group.png',
                                                      height: _media.height / 10,
                                                      width: _media.width/5,
                                                    ),
                                                    SizedBox(
                                                      height: _media.height / 50,
                                                    ),
                                                    Container(
                                                      width: _media.width/5,
                                                      padding: EdgeInsets.all(0),
                                                      decoration: BoxDecoration(
                                                        gradient: LinearGradient(
                                                          begin: Alignment.topCenter,
                                                          end: Alignment(0, 0.8),
                                                          colors: [
                                                            const Color(0xFFEBB395),
                                                            const Color(0xFFBE744F)
                                                          ] , // whitish to gray
                                                        ),
                                                        borderRadius: BorderRadius.circular(5),
                                                      ),
                                                      child: Text(
                                                        '\$9.99',
                                                        textAlign: TextAlign.center,
                                                        style: TextStyle(
                                                          fontFamily: "td-neumann",
                                                          color: Colors.black,
                                                          fontSize: 20,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                )
                                            ),
                                          ),

                                          Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
//                                                mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Text(
                                                "CERTIFICATE",
                                                style: TextStyle(
                                                  fontFamily: "td-neumann",
                                                  color: Colors.white,
                                                  fontSize: 20,
                                                ),
                                              ),
                                              SizedBox(
                                                height: _media.height/50,
                                              ),
                                              Container(
                                                width: _media.width*3/7,
                                                child: Text(
                                                  "This is the one hell of a Didonesinspired - sturdy and \"mechanical\" Neumann.It all started with something good.",
                                                  textAlign: TextAlign.justify,
                                                  softWrap: true,
                                                  maxLines: 10,
                                                  style: TextStyle(
                                                    color: Color(0xFF7B655A),
                                                  ),
                                                ),
                                              ),


                                            ],
                                          ),

                                          Container(
                                            margin: EdgeInsets.fromLTRB(_media.width/25,0,0,0),
                                            padding: EdgeInsets.all(10),
                                            decoration: BoxDecoration(
                                              color: Color(0xFFBE744F),
                                              shape: BoxShape.circle,
                                              gradient: LinearGradient(
                                                begin: Alignment.topCenter,
                                                end: Alignment(0, 0.8),
                                                colors: [
                                                  const Color(0xFFEBB395),
                                                  const Color(0xFFBE744F)
                                                ] , // whitish to gray
                                              ),
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.symmetric(vertical: 15,  horizontal: 0),
                                              child:  Text(
                                                '2',
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  fontFamily: "td-neumann",
                                                  color: Colors.black,
                                                  fontSize: 25,
                                                ),
                                              ),
                                            ),

                                          ),
                                        ],

                                      ),

                                      /* child:Row(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.fromLTRB(_media.width/25,0,0,0),
                                          child: Image.asset("./assets/images/stick2.png",
                                            width: 10.0,
//                                            height: _media.height/2,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                          const EdgeInsets.symmetric(vertical: 0.0, horizontal: 10),
                                          child: (
                                              Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[

                                                  Image.asset(
                                                    './assets/images/Group.png',
                                                    height: _media.height / 10,
                                                    width: _media.width/5,
                                                  ),
                                                  SizedBox(
                                                    height: _media.height / 50,
                                                  ),
                                                  Container(
                                                    width: _media.width/5,
                                                    padding: EdgeInsets.all(0),
                                                    decoration: BoxDecoration(
                                                      gradient: LinearGradient(
                                                        begin: Alignment.topCenter,
                                                        end: Alignment(0, 0.8),
                                                        colors: [
                                                          const Color(0xFFEBB395),
                                                          const Color(0xFFBE744F)
                                                        ] , // whitish to gray
                                                      ),
                                                      borderRadius: BorderRadius.circular(5),
                                                    ),
                                                    child: Text(
                                                      '\$9.99',
                                                      textAlign: TextAlign.center,
                                                      style: TextStyle(
                                                        fontFamily: "td-neumann",
                                                        color: Colors.black,
                                                        fontSize: 20,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              )
                                          ),
                                        ),

                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.center,

                                          children: <Widget>[
                                            SizedBox(
                                              height: _media.height / 50,
                                            ),
                                            Container(
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    "OPTION TWO",
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      letterSpacing: 4,

//                                                fontFamily: "td-neumann",
                                                      color: Colors.white,
                                                      fontSize: 10,
                                                    ),
                                                  ),

                                                ],
                                              ),
                                            ),

                                            Text(
                                              "CERTIFICATE",
                                              style: TextStyle(
                                                fontFamily: "td-neumann",
                                                letterSpacing: 4,
                                                color: Colors.white,
                                                fontSize: 20,
                                              ),
                                            ),
                                            SizedBox(
                                              height: _media.height/50,
                                            ),
                                            Container(
                                              width: _media.width*3/7,
                                              child: Text(
                                                "This is the one hell of a Didonesinspired - sturdy and \"mechanical\" Neumann.It all started with something good.",
                                                textAlign: TextAlign.justify,
                                                softWrap: true,
                                                maxLines: 10,
                                                style: TextStyle(
                                                  color: Color(0xFF7B655A),
                                                ),
                                              ),
                                            ),


                                          ],
                                        ),

                                        Container(
                                          margin: EdgeInsets.fromLTRB(_media.width/25,_media.width/25,0,0),
                                          padding: EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                            color: Color(0xFFBE744F),
                                            shape: BoxShape.circle,
                                            gradient: LinearGradient(
                                              begin: Alignment.topCenter,
                                              end: Alignment(0, 0.8),
                                              colors: [
                                                const Color(0xFFEBB395),
                                                const Color(0xFFBE744F)
                                              ] , // whitish to gray
                                            ),
                                          ),
                                          child: Padding(
                                            padding: EdgeInsets.symmetric(vertical: 0,  horizontal: 0),
                                            child: FittedBox(
                                              fit: BoxFit.contain,
                                              child: Text(
                                                '2',
                                                style: TextStyle(
                                                  fontFamily: "td-neumann",
                                                  color: Colors.black,
                                                  fontSize: 25,
                                                ),
                                              ),
                                            ),

                                          ),

                                        ),
                                      ],

                                    ),*/

                                    ),
                                  )
                              ),
                              Container(
                                padding: EdgeInsets.fromLTRB(0, _media.height/4-10,0,0),
                                child:  Stack(
                                  children: <Widget>[
                                    Container(
                                        height: _media.height/4,
                                        child: InkWell(
                                          onTap: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => ThirdPage(
                                                    first_name: widget.first_name,
                                                    last_name: widget.last_name,
                                                    index: "3",
                                                    url: './assets/images/diamond.png',
                                                  )),
                                            );
                                          },
                                          child: Container(
                                            height: _media.height / 4,
                                            margin: EdgeInsets.symmetric(
                                                vertical: 0, horizontal: _media.width / 20),
                                            decoration: BoxDecoration(
                                              gradient: LinearGradient(
                                                begin: Alignment.topLeft,
                                                end: Alignment(0.8, 0.0),
                                                // 10% of the width, so there are ten blinds.
                                                colors: [
                                                  const Color(0xFF372C29),
                                                  const Color(0xFF1B1C20)
                                                ], // whitish to gray
                                              ),
                                              borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(10),
                                                topRight: Radius.circular(10),
                                                bottomLeft: Radius.circular(0),
                                                bottomRight: Radius.circular(0),
                                              ),
                                              border: Border.all(
                                                width: 0.0,
                                              ),
                                            ),

                                            child:Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                  padding: EdgeInsets.fromLTRB(_media.width/25,0,0,0),
                                                  child: Image.asset("./assets/images/stick3.png",
                                                    width: 10.0,
                                                  ),
                                                ),
                                                Padding(
                                                  padding:
                                                  const EdgeInsets.symmetric(vertical: 0.0, horizontal: 10),
                                                  child: (
                                                      Column(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        children: <Widget>[
                                                          Image.asset(
                                                            './assets/images/diamond.png',
                                                            height: _media.height / 10,
                                                            width: _media.width/5,
                                                          ),
                                                          SizedBox(
                                                            height: _media.height / 50,
                                                          ),
                                                          Container(
                                                            width: _media.width/5,
                                                            padding: EdgeInsets.all(0),
                                                            decoration: BoxDecoration(
                                                              gradient: LinearGradient(
                                                                begin: Alignment.topCenter,
                                                                end: Alignment(0, 0.8),
                                                                colors: [
                                                                  const Color(0xFFEBB395),
                                                                  const Color(0xFFBE744F)
                                                                ] , // whitish to gray
                                                              ),
                                                              borderRadius: BorderRadius.circular(5),
                                                            ),
                                                            child: Text(
                                                              '\$19.99',
                                                              textAlign: TextAlign.center,
                                                              style: TextStyle(
                                                                fontFamily: "td-neumann",
                                                                color: Colors.black,
                                                                fontSize: 20,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      )
                                                  ),
                                                ),

                                                Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
//                                                mainAxisSize: MainAxisSize.min,
                                                  children: <Widget>[
                                                    Text(
                                                      "CERTIFICATE",
                                                      style: TextStyle(
                                                        fontFamily: "td-neumann",
                                                        color: Colors.white,
                                                        fontSize: 20,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: _media.height/50,
                                                    ),
                                                    Container(
                                                      width: _media.width*3/7,
                                                      child: Text(
                                                        "This is the one hell of a Didonesinspired - sturdy and \"mechanical\" Neumann.It all started with something good.",
                                                        textAlign: TextAlign.justify,
                                                        softWrap: true,
                                                        maxLines: 10,
                                                        style: TextStyle(
                                                          color: Color(0xFF7B655A),
                                                        ),
                                                      ),
                                                    ),


                                                  ],
                                                ),

                                                Container(
                                                  margin: EdgeInsets.fromLTRB(_media.width/25,0,0,0),
                                                  padding: EdgeInsets.all(10),
                                                  decoration: BoxDecoration(
                                                    color: Color(0xFFBE744F),
                                                    shape: BoxShape.circle,
                                                    gradient: LinearGradient(
                                                      begin: Alignment.topCenter,
                                                      end: Alignment(0, 0.8),
                                                      colors: [
                                                        const Color(0xFFEBB395),
                                                        const Color(0xFFBE744F)
                                                      ] , // whitish to gray
                                                    ),
                                                  ),
                                                  child: Padding(
                                                    padding: EdgeInsets.symmetric(vertical: 15,  horizontal: 0),
                                                    child:  Text(
                                                      '3',
                                                      style: TextStyle(
                                                        fontFamily: "td-neumann",
                                                        color: Colors.black,
                                                        fontSize: 25,
                                                      ),
                                                    ),
                                                  ),

                                                ),
                                              ],

                                            ),

                                          ),
                                        )
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),

                      ],
                    ),
                  ),
                ],
              ),
//            Image.asset("./assets/images/dollar.png"),
            ],
          ),
    ),
      ),
    );
  }
}
