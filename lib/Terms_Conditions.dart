import 'package:flutter/material.dart';
import 'package:flutter_plugin_pdf_viewer/flutter_plugin_pdf_viewer.dart';

class TermsCond extends StatefulWidget {
  @override
  _TermsCondState createState() => _TermsCondState();
}

class _TermsCondState extends State<TermsCond> {
  bool _isLoading = true;
  PDFDocument document;
  @override
  void initState() {
    super.initState();
    loadDocument();
  }
  loadDocument() async {
    document = await PDFDocument.fromURL(
        "https://s3-us-west-2.amazonaws.com/onlyrichpeople/Terms_Conditions.pdf");
    setState(() => _isLoading = true);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
            child: PDFViewer(document: document,showPicker: false,showIndicator: false, tooltip: PDFViewerTooltip(first:"Batatas"))),
      ),
    );
  }
}