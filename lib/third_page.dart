import 'package:flutter/material.dart';
import 'package:flutter_task/login.dart';

import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:screenshot/screenshot.dart';
import 'package:auto_size_text/auto_size_text.dart';

class ThirdPage extends StatefulWidget {
  GlobalKey globalKey = GlobalKey();

  String first_name = "";
  String last_name = "";
  String index = "";
  String url = "";
  ThirdPage({this.first_name, this.last_name, this.index, this.url });

  @override
  _ThirdPageState createState() => _ThirdPageState();
}

class _ThirdPageState extends State<ThirdPage> {
  String _imageFile;
  ScreenshotController screenshotController = ScreenshotController();
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final _media = MediaQuery.of(context).size;
    return Scaffold(
      body: Screenshot(
        controller: screenshotController,
        child: Container(
          height: double.infinity / 2,
          width: double.infinity / 2,
          decoration: BoxDecoration(
//          gradient: SIGNUP_BACKGROUND,
            color: Color(0xE9000000),
//          color: Color(0xE9FFFFFF),
          ),
          child: Stack(
            children: <Widget>[
              Positioned(
                top: 10,
                right: 40,
                child: Image.asset(
                  './assets/images/dollar.png',
                  height: _media.height / 4,
                ),
              ),
              Positioned(
                top: _media.height/15,
                right: 5,
                child: Container(
                  height: _media.height/30,
                  padding: EdgeInsets.all(0),
                  decoration: BoxDecoration(
                    color: Color(0xFFBE744F),
                    shape: BoxShape.circle,
                  ),
                  child: Image.asset("./assets/images/close.png"),
                  /*IconButton(
                  icon: Icon(Icons.close, size: 20,),
                  color: Colors.blue,
//                              onPressed: () {Navigator.of(context).pop();}
                ),*/
                ),
              ),
              Column(
                children: <Widget>[
                  Expanded(
                    flex: 10,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        SizedBox(
                          height: _media.height / 20,
                        ),
                        Container(
                          margin: EdgeInsets.all(_media.height/100),
                          height: _media.height*1.5/20,
                          width: _media.width*3.5/5,
                          child: FittedBox(
                            child: Text(
                              'CONGRATULATIONS',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontFamily: "td-neumann",
                                color: Colors.white,
//                                fontSize: 30,
                              ),
                            ),
                          ),

                        ),

                        Column(
                          children: <Widget>[
                            Container(
                              height: _media.height / 5,
                              width: _media.width *3/ 5,

                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment(0.8, 0.0),
                                  // 10% of the width, so there are ten blinds.
                                  colors: [
                                    const Color(0xFF372C29),
                                    const Color(0x001B1C20)
                                  ], // whitish to gray
                                ),
                                border: Border.all(
                                  color: Color(0xFFBE744F),
                                  width: 2.0,
                                ),
                              ),
                                   child: FittedBox(
                                     child:  Text(
                                       "MASTER\n" + widget.first_name+ "\n" + widget.last_name,
                                       textAlign: TextAlign.center,
                                       style: TextStyle(
                                         fontFamily: "td-neumann",
//                                         height: 0.9,
                                         color: Color(0xFFE6B898),
//                                        fontSize: 40,

                                       ),
                                     ),
                                   ),
                            ),
                            Image.asset(
                              './assets/images/Combined Shape.png',
                              height: _media.height / 20,
                            ),
                          ],
                        ),
                        Container(
                          height: _media.height*11/200,
                          child: FittedBox(
                            child: Text(
                              'YOU EARNED',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontFamily: "td-neumann",
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 4, // 20%
                    child: Container(
                      height: _media.height / 5,
                      margin: EdgeInsets.symmetric(
                          vertical: 0, horizontal: _media.width / 20),
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.bottomRight,
                          end: Alignment(0, 0),
                          colors: [
                            const Color(0x99BE744F),
                            const Color(0xFF372C29),
                          ], // whitish to gray
                        ),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                          bottomLeft: Radius.circular(10),
                          bottomRight: Radius.circular(10),
                        ),
                        border: Border.all(
                          width: 0.0,
                        ),
                      ),


                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[

                            Container(
                              height: _media.height/5,
                              width: _media.width/4,
                              child: Center(
                                child:  Image.asset(
                                  widget.url,
                                  height: _media.height / 10,
                                  width: _media.width/5,
                                ),
                              ),
                            ),

                            Container(
                              height: _media.height/5,
                              width: _media.width/2,

                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,

                                children: <Widget>[

                                  /* Container(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          "OPTION TWO",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                            letterSpacing: 4,

//                                                fontFamily: "td-neumann",
                                            color: Colors.white,
                                            fontSize: 10,
                                          ),
                                        ),

                                      ],
                                    ),
                                  ),*/

                                  Container(
                                    height: _media.height/20,
                                    child: FittedBox(
                                      child: Text(
                                        "CERTIFICATE",
                                        style: TextStyle(
                                          fontFamily: "td-neumann",
                                          letterSpacing: 4,
                                          color: Colors.white,
//                                    fontSize: 30,
                                        ),
                                      ),
                                    ),
                                  ),

                                  SizedBox(
                                    height: _media.height/80,
                                  ),
                                  Container(
//                                    width: _media.width*3/7,
                                    height: _media.height*2.5/20,
//                                    child: FittedBox(
                                      child: AutoSizeText(
                                        "This is the one hell of a Didonesinspired - sturdy and \"mechanical\" Neumann.It all started with something good.",
                                        textAlign: TextAlign.justify,
                                        softWrap: true,
                                      maxLines: 4,
                                        minFontSize: 3.0,
                                        style: TextStyle(
                                          color: Color(0xFF7B655A),
                                        ),
                                      ),
//                                    ),

                                  ),


                                ],
                              ),
                            ),


                            Container(
                              margin: EdgeInsets.fromLTRB(_media.width/50,_media.width/20,0,0),
                              padding: EdgeInsets.all(10),
//                              height: _media.height/5,
                              width: _media.width*2/20,
                              decoration: BoxDecoration(
                                color: Color(0xFFBE744F),
                                shape: BoxShape.circle,
                                gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment(0, 0.8),
                                  colors: [
                                    const Color(0xFFEBB395),
                                    const Color(0xFFBE744F)
                                  ] , // whitish to gray
                                ),
                              ),
                                  child: FittedBox(
                                    child: Text(
                                      widget.index,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontFamily: "td-neumann",
                                        color: Colors.black,
                                      fontSize: 25,
                                      ),
                                    ),
                                  ),



                            ),
                          ],

                        ),


                    ),
                  ),
                  Expanded(
                    flex: 6,
                    child: Container(
                      height: _media.height*6/20,
                      width: _media.width*5/5,

                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[

                            SizedBox(
                              height: _media.height/80,
                            ),
                            Container(
                              height: _media.height/20,
                              child: FittedBox(
                               child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "YOUR RANK IS  ",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontFamily: "td-neumann",
                                        color: Colors.white,
//                                        fontSize: 20,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 5),
                                      decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                          begin: Alignment.topCenter,
                                          end: Alignment(0, 0.8),
                                          colors: [
                                            const Color(0xFFEBB395),
                                            const Color(0xFFBE744F)
                                          ] , // whitish to gray
                                        ),
                                        color: Color(0xFFBE744F),
                                        shape: BoxShape.rectangle,
                                        borderRadius: BorderRadius.circular(5.0),
                                      ),
                                      child: Text(
                                        '876',
                                        style: TextStyle(
                                          fontFamily: "td-neumann",
                                          color: Colors.black,
//                                          fontSize: 20,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: _media.height/80,
                            ),
                            Container(
                              height: _media.height*1.5/20,
                              width: _media.width/4,
                              child: FittedBox(
                                child:  Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "1 L lord louis Kahn",
                                      style: TextStyle(color: Color(0xFF7B655A)),
                                    ),
                                    Text(
                                      "2 Dr Arthur Kayne",
                                      style: TextStyle(color: Color(0xFF7B655A)),
                                    ),
                                    Text(
                                      "3 Steve John",
                                      style: TextStyle(color: Color(0xFF7B655A)),
                                    ),
                                    Text(
                                      "...",
                                      textAlign: TextAlign.start,
                                      style: TextStyle(color: Color(0xFF7B655A)),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: _media.height/80,
                            ),
                            Container(
                              height: _media.height/20,
                                child: Container(
                                  width: _media.width*3/5,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment.topLeft,
                                      end: Alignment(0.7, 0.0),
                                      // 10% of the width, so there are ten blinds.
                                      colors: [
                                        const Color(0xFF372C29),
                                        const Color(0xFF1B1C20)
                                      ], // whitish to gray
                                    ),
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(5.0),
                                    border: Border.all(
                                      color: Color(0xFFBE744F),
                                      width: 1.0,
                                    ),
                                  ),
                                  child:  FittedBox(
                                    child:Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        " 876",
                                        style: TextStyle(
                                          color: Color(0xFFE0A484),
                                          fontFamily: "td-neumann",
                                          fontWeight: FontWeight.bold,
//                                          fontSize: 15,
                                        ),
                                      ),
                                      Text(
                                        "  Master " + widget.first_name + " ",
                                        style: TextStyle(
                                          fontFamily: "td-neumann",
                                          color: Colors.white,
//                                          fontSize: 15,
                                        ),
                                      ),
                                      Text(
                                        widget.last_name,
                                        style: TextStyle(
                                          fontFamily: "td-neumann",
                                          color: Colors.white,
//                                          fontSize: 15,
                                        ),
                                      ),
                                    ],
                                  ),
                              ),

                                ),

                            ),
                            SizedBox(
                              height: _media.height/80,
                            ),
                            Container(
                              height: _media.height/20,
                              child: FittedBox(
                                child: Container(
                                  width: _media.width,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Text("S H A R E",
                                        style: TextStyle(
                                          fontFamily: "td-neumann",
                                          color: Color(0xFF7B655A),
                                          fontSize: 15,
                                        ),
                                      ),
                                      GestureDetector(
                                        child: Image.asset(
                                          './assets/images/twitter.png',
                                          height: _media.height / 20,
//                          width: _media.width ,
                                        ),
                                        onTap:() {
//                                          const CircularProgressIndicator();
                                          screenshotController.capture().then((File image) {
                                            //Capture Done
                                            setState(() {
//                                      print(image);
                                              _shareImage(image);
                                            });
                                          }).catchError((onError) {
                                            print(onError);
                                          }
                                          );
                                        },
                                      ),
                                      GestureDetector(
                                        child: Image.asset(
                                          './assets/images/facebook.png',
                                          height: _media.height / 20,
//                          width: _media.width ,
                                        ),
                                        onTap:() {
                                          screenshotController.capture().then((File image) {
                                            //Capture Done
                                            setState(() {
//                                      print(image);
                                              _shareImage(image);
                                            });
                                          }).catchError((onError) {
                                            print(onError);
                                          }
                                          );
                                        },
                                      ),
                                      GestureDetector(
                                        child: Image.asset(
                                          './assets/images/instagram.png',
                                          height: _media.height / 20,
//                          width: _media.width ,
                                        ),
                                        onTap:() {
                                          screenshotController.capture().then((File image) {
                                            //Capture Done
                                            setState(() {
//                                      print(image);
                                              _shareImage(image);
                                            });
                                          }).catchError((onError) {
                                            print(onError);
                                          }
                                          );
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),

                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      )
    );
  }

  Future _shareImage(File file) async {
    try {
      await Share.file(
          'only rich people', 'ranking.png', file.readAsBytesSync(), 'image/png');
    } catch (e) {
      print('error: $e');
    }
  }


}
