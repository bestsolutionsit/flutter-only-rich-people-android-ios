import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter_task/second_page.dart';
import 'package:flutter_task/Privacy_Policy.dart';
import 'package:flutter_task/Terms_Conditions.dart';
import 'package:flutter/gestures.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  bool _autoValidate = false;
  String _firstname;
  String _lastname;
  String _email;

  TapGestureRecognizer _terms;
  TapGestureRecognizer _privacy;

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  String validateName(String value) {
    Pattern pattern = r"^[a-zA-Z]+(([',.-][a-zA-Z])?[a-zA-Z]*)*$";
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Name';
    else
      return null;
  }

  var _check = false;

  void _submitCommand() {
    final form = _formKey.currentState;

    if (form.validate()) {
      form.save();
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => SecondPage(
                  first_name: this._firstname,
                  last_name: this._lastname,
                )
        ),
      );
    }
  }

  void _checkChanged(val) => setState(() => _check = val);

  @override

  void initState(){
    _terms = TapGestureRecognizer()
        ..onTap = (){
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => TermsCond(),
                )
          );
        };
    _privacy = TapGestureRecognizer()
      ..onTap = (){
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PrivacyPolicy(),
            )
        );
      };
  }

  Widget build(BuildContext context) {
    final _media = MediaQuery.of(context).size;
    var underlineStyle = TextStyle(
      decoration: TextDecoration.underline,
      fontWeight: FontWeight.bold,
      color: Color(0xFFE6B898),
      height: 1.5,
      );
    return Scaffold(
      body: Container(
        height: double.infinity / 2,
        width: double.infinity / 2,
        decoration: BoxDecoration(
//          gradient: SIGNUP_BACKGROUND,
          color: Color(0xE9000000),
//          color: Color(0xE9FFFFFF),
        ),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Stack(
            children: <Widget>[
              Positioned(
                top: 10,
                right: 40,
                child: Image.asset(
                  './assets/images/dollar.png',
                  height: _media.height / 4,
                ),
               /*  child: Text("\$",
                style: TextStyle(
                  fontFamily: "td-neumann",
                  fontSize: 100,
                  color: Colors.black54,
                ),
                ),*/
              ),
              Padding(
                padding:
                    EdgeInsets.fromLTRB(_media.width/20,80,_media.width/20,80),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Image.asset(
                              './assets/images/logo.PNG',
                              height: _media.height / 5,
                              width: _media.width*3.5/5,
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 15.00,
                        ),

                      ],
                    ),
                    Image.asset(
                      './assets/images/line.PNG',
                      height: _media.height / 25,
                      width: _media.width*4/5,

                    ),
                    SizedBox(
                      height: _media.height / 15,
                    ),
                    Container(
//                          height: _media.height / 3,
                      decoration: BoxDecoration(
//                            gradient: SIGNUP_CARD_BACKGROUND,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                textCapitalization: TextCapitalization.characters,
                                decoration: const InputDecoration(
                                  hintStyle: TextStyle(
                                    color: Color(0xFFBE744F),
                                  ),
                                  hintText: 'First Name',
                                  enabledBorder: const OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Color(0xFFBE744F), width: 1.0),
                                  ),
                                ),
                                style: TextStyle(
                                  color: Color(0xFFBE744F),
                                ),
                                keyboardType: TextInputType.text,
                                validator: validateName,
                                onSaved: (String val) {
                                  _firstname = val.toUpperCase();
                                },
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                textCapitalization: TextCapitalization.characters,
                                decoration: const InputDecoration(
                                  hintStyle: TextStyle(
                                    color: Color(0xFFBE744F),
                                  ),
                                  hintText: 'Last Name',
                                  enabledBorder: const OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Color(0xFFBE744F), width: 1.0),
                                  ),
                                ),
                                style: TextStyle(
                                  color: Color(0xFFBE744F),
                                ),
                                keyboardType: TextInputType.text,
                                validator: validateName,
                                onSaved: (String val) {
                                  _lastname = val.toUpperCase();
                                },
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextFormField(
                                decoration: const InputDecoration(
                                  hintStyle: TextStyle(
                                    color: Color(0xFFBE744F),
                                  ),
                                  hintText: 'Email',
                                  enabledBorder: const OutlineInputBorder(
                                    borderSide: const BorderSide(
                                        color: Color(0xFFBE744F), width: 1.0),
                                  ),
                                ),
                                style: TextStyle(
                                  color: Color(0xFFBE744F),
                                ),
                                keyboardType: TextInputType.emailAddress,
                                validator: validateEmail,
                                onSaved: (String val) {
                                  _email = val;
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          width: 21,
                          height: 21,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Color(0xFFE6B898),
                              width: 2.0,
                              style: BorderStyle.solid,
                            ),
                          ),
                          child: Checkbox(
                            value: _check,
                            activeColor: Color(0xE9000000),
                            checkColor: Color(0xFFE6B898),
                            onChanged: _checkChanged,
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: _media.width*4/5,
                              child:  RichText(
                                textAlign: TextAlign.center ,
                                text: TextSpan(
                                  text: 'I accept the ',
                                  style: underlineStyle.copyWith(decoration: TextDecoration.none),
                                  children: <TextSpan>[
                                    TextSpan(text: 'Terms and Conditions ', style: underlineStyle, recognizer: _terms),
                                    TextSpan(text: 'and '),
                                    TextSpan(text: 'Privacy policy', style: underlineStyle, recognizer: _privacy),
                                  ],
                                ),
                             /* Text(
                                'I accept the Terms and Conditions and Privacy policy',
                                textAlign: TextAlign.center,
                                style: TextStyle(
//                                   fontWeight: FontWeight.bold,
                                  color: Color(0xFFE6B898),
                                ),
//                                textAlign: TextAlign.left,
                                softWrap: true,
                              ),*/
                            ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: _media.height/15,
                    ),
                GestureDetector(

                    child: Container(
                      height: _media.height / 15,
                      width: _media.width,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment(0, 0),
                          colors: _check? [
                            const Color(0xFFBE744F),
                            const Color(0xFFEBB395)
                          ] : [
                             Colors.white12,
                             Colors.white12
                          ], // whitish to gray
                        ),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Center(
                        child: Text("CONTINUE",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'td-neumann',
                        fontSize: 20,
                      ),
                    ),
                ),

                    ),

                    onTap: _check? () {
                    _submitCommand();
                  }: null),

                    /*SizedBox(
                        width: _media.width , // match_parent
                        child: RaisedButton(
                            child: const Text('CONTINUE',
                            style: TextStyle(fontFamily: 'Georgia', fontSize: 20),
                            ),
                            color: Color(0xFFBE744F),
                            elevation: 4.0,
                            splashColor: Colors.orange,
                            disabledColor: Colors.white12,
                            onPressed: _check
                                ? () {
                                    _submitCommand();
                                  }
                                : null)),*/
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
/* Widget inputText(
      String fieldName,
      String hintText,
      TextEditingController controller,
      bool obSecure,
      ) {
    return TextField(
      style: TextStyle(height: 0.3, color: Colors.yellow),
      keyboardType: TextInputType.emailAddress,
      controller: controller,

      decoration: InputDecoration(
        enabledBorder: const OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.yellow, width: 1.0),
        ),
        hintText: hintText,
        labelText: fieldName,
        labelStyle: TextStyle(
//          fontSize: TEXT_NORMAL_SIZE,
          color: Colors.yellow,
          fontFamily: "Montserrat",
          fontWeight: FontWeight.w400,
          letterSpacing: 1,
          height: 1,
        ),
        border: OutlineInputBorder(

          borderRadius: const BorderRadius.all(
            const Radius.circular(6.00)
          ),
        ),
      ),
      obscureText: obSecure,
    );
  }*/

}
