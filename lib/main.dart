import 'package:flutter/material.dart';
import 'package:flutter_task/login.dart';

//import 'package:freelancer_flutter/second.dart';
//import 'package:freelancer_flutter/third.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Only Rich People',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),
//    home: SecondPage(),
    );
  }
}
